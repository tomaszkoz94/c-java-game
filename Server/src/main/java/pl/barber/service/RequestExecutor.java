package pl.barber.service;

import pl.barber.database.Database;

public class RequestExecutor {

    private Database database;

    public RequestExecutor() {
        this.database = Database.getInstance();
    }


    public synchronized String getXML(){
        return database.getBankInXML();
    }

    public void setNewPrices(){
        database.setNewRandomPrices();
    }

}
