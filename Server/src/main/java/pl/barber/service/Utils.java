package pl.barber.service;

public class Utils {
    public static boolean isEmpty(Object object){
        if(object==null){
            return true;
        }
        return false;
    }


    public boolean isBlank(String str){
        if(str==null)
            return false;
        str.replace("\\s", "");
        if ("".equals(str))
           return false;
        return true;
    }

    public static final String DATABASE_LOGGER = "";
    public static final String CONNECTION_LOGGER = "Connection: ";
    public static final String REQUEST_EXECUTOR_LOGGER = "Request: ";
}
