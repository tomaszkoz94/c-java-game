package pl.barber;

import pl.barber.connector.Connector;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        int port = 1133;
        new Thread(new Connector(port)).start();

    }
}
