package pl.barber.connector;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Connector implements Runnable{
    private int port;
    private ServerSocket listener;
    private int clientNumber;
    private List<Connection> connectionList;
    private ServerSocket serversocket;
    private ResourceLock lock;

    public Connector(int port) {
        this.port = port;
        lock = new ResourceLock();
        lock.integers = new ArrayList<>();
        clientNumber = 0;
        connectionList = new LinkedList<>();
        try {
            serversocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run(){
        while(true){
            try {
                Socket client = serversocket.accept();
                Connection connection = new Connection(client, clientNumber, connectionList, lock);
                connectionList.add(connection);
                Thread t = new Thread(connection);
                t.start();
                clientNumber++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



}
