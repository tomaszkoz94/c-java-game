package pl.barber.connector;

import pl.barber.database.Bank;
import pl.barber.database.ClientBank;
import pl.barber.database.Database;

import java.util.ArrayList;
import java.util.List;

public class Account {
    private static List<ClientBank> ownBankList;

    private double totalMoney;

    private static Database database;
    public Account() {
        ownBankList = initBanks();
        totalMoney = 400;
    }

    public double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(double totalMoney) {
        this.totalMoney = totalMoney;
    }

    private List<ClientBank> initBanks() {
        List<ClientBank> banks = new ArrayList<>();
        database = Database.getInstance();
        for (int i = 0; i < 7; i++) {
            banks.add(new ClientBank(database.getBankById(i), i));
        }
        return banks;
    }

    public void updateData(String request) {
        String[] requestCommands = request.split("#");
        database = Database.getInstance();
        int i = 0;
        for (String singleCommand : requestCommands) {
            execute(singleCommand, i, database);
            i++;
        }
    }

    private void execute(String command, int bankNumber, Database database) {
        char operationType = command.charAt(0);
        int units = Integer.parseInt(command.substring(1));
        int actualUnits = getUnitsOfBank(bankNumber);
        double newMoneyValue;
        if (operationType == 'B'){
            //Buy
            ownBankList.set(bankNumber, new ClientBank(database.getBankById(bankNumber), actualUnits + units));
            newMoneyValue = getTotalMoney() - (units * database.getBankById(bankNumber).getPrice());
            setTotalMoney(newMoneyValue);

        }
        if (operationType == 'S'){
            //Sell
            ownBankList.set(bankNumber, new ClientBank(database.getBankById(bankNumber), actualUnits - units));
            newMoneyValue = getTotalMoney() + (units * database.getBankById(bankNumber).getPrice());
            setTotalMoney(newMoneyValue);
        }
    }

    private static int getUnitsOfBank(int bankNumber) {
        return ownBankList.get(bankNumber).getUnits();
    }

    public String getAccountBalance(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<account>");
        for (ClientBank oneBank : ownBankList){
            stringBuilder.append("<" + oneBank.getBank().getName() + ">" + oneBank.getUnits() + "</" + oneBank.getBank().getName() + ">");
        }
        stringBuilder.append("<money>" + getTotalMoney() +"</money>");
        stringBuilder.append("</account>");
        return  stringBuilder.toString();
    }
}
