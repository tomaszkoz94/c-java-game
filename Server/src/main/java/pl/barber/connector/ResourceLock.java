package pl.barber.connector;

import java.util.List;

public class ResourceLock {
    public volatile int flag = 0;
    public volatile List<Integer> integers;

    boolean isAllTheSame(){
        int first = integers.get(0);
        for (int i = 0; i < integers.size(); i++){
            if (integers.get(i) != first){
                return false;
            }
        }
        return true;
    }
}
