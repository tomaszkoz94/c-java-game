package pl.barber.connector;

import pl.barber.Contact;
import pl.barber.database.Bank;
import pl.barber.database.Database;
import pl.barber.service.RequestExecutor;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Connection implements Runnable {
    private Socket client;
    private PrintWriter pw;
    private Scanner scanner;
    private List<Connection> connectionList;
    private RequestExecutor requestExecutor;
    private int clientNumber;
    private Account account;
    private ResourceLock lock;

    public Connection(Socket socket, int clientNumber, List<Connection> connectionList, ResourceLock lock) {
        System.out.println("New Connection");
        requestExecutor = new RequestExecutor();
        this.lock = lock;
        lock.integers.add(0);
        this.connectionList = connectionList;
        this.client = socket;
        this.account = new Account();
        this.clientNumber = clientNumber;
        try {
            scanner = new Scanner(this.client.getInputStream());
            pw = new PrintWriter(this.client.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //wyslij
        String toSend = "<root>" + requestExecutor.getXML() + account.getAccountBalance() + "</root>";
        pw.println(toSend);
    }

    @Override
    public void run() {
        System.out.println("Contacts server is ready ....");
        while (true) {
            System.out.println("-------------------------");
            System.out.println("lock flag up: "+lock.flag);
            System.out.println("Client number: "+ clientNumber);

            try {
                try {
                    synchronized (lock) {
                        while ((lock.flag != clientNumber) && lock.isAllTheSame()) {
                            lock.wait();
                        }
                        //odbierz
                        String request = scanner.nextLine();
                        //przetworz
                        account.updateData(request);
                        //wyslij
                        String toSend = "<root>" + requestExecutor.getXML() + account.getAccountBalance() + "</root>";
                        pw.println(toSend);
                        Thread.sleep(1000);
                        lock.flag++;
                        if (lock.flag > connectionList.size() - 1) {
                            requestExecutor.setNewPrices();
                            lock.flag = 0;
                        }
                        lock.notifyAll();
                        lock.integers.set(clientNumber, lock.integers.get(clientNumber) + 1);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //odbierz
                System.out.println("lock flag down: "+ lock.flag);
                System.out.println("Connection list length: " + connectionList.size());
                System.out.println("******************************");
            }catch (Exception e){
                e.printStackTrace();
                connectionList.remove(this);
                break;
            }
        }
    }
}












