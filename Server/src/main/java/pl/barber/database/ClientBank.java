package pl.barber.database;

public class ClientBank {
    private Bank bank;
    private int units;

    public ClientBank(Bank bank, int units){
        this.bank = bank;
        this.units = units;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }
}
