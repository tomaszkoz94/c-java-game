package pl.barber.database;

import java.time.LocalTime;
import java.util.*;

public class Database {
    private static Database instance;

    private static List<Bank> banks;

    private Database() {
        banks = new ArrayList<>();
        InitBankList();
    }

    private void InitBankList() {
        banks.add(new Bank("ALIOR", 70));
        banks.add(new Bank("BOS", 15));
        banks.add(new Bank("HANDLOWY", 20));
        banks.add(new Bank("MILLENIUM", 50));
        banks.add(new Bank("MBANK", 100));
        banks.add(new Bank("PEKAO", 80));
        banks.add(new Bank("IDEABANK", 40));
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    public Bank getBankById(int id){
        return banks.get(id);
    }

    public void setNewRandomPrices() {
        Random random = new Random();
        int min = -80;
        int max = 100;
        for (Bank bank : banks){
            double newPrice = bank.getPrice() + random.nextInt((max - min) + 1) + min;
            if (newPrice < 1){
                bank.setPrice(1);
            }
            else {
                bank.setPrice(newPrice);
            }
        }
    }

    public String getBankInXML(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<banks>");
        for (Bank bank : banks){
            stringBuilder.append("<" + bank.getName() + ">" + bank.getPrice() + "</" + bank.getName() + ">");
        }
        stringBuilder.append("</banks>");
        return stringBuilder.toString();
    }


    private String getUUID() {
        return UUID.randomUUID().toString();
    }

    private static final LocalTime START_TIME = LocalTime.of(10, 0);

    private static final LocalTime FINISH_TIME = LocalTime.of(18, 0);
}
