package pl.barber.service;

import org.junit.Before;
import org.junit.Test;
import pl.barber.exceptions.InvalidRequestFormatException;
import pl.barber.model.Request;
import pl.barber.model.RequestType;
import pl.barber.model.Service;
import pl.barber.service.RequestExecutor;

import java.time.LocalDateTime;

public class RequestExecutorTest {
    @Before
    public void init(){
        requestChecker = new RequestExecutor();
    }
    @Test
    public void executeRequestTest(){
        Object request = new Request();
        ((Request) request).setRequestType(RequestType.POST);
        Service service = new Service();
        service.setCustomerName("qwe");
        service.setId("qwe");
        service.setStartTime(LocalDateTime.now());
        ((Request) request).setService(service);

    }

    private RequestExecutor requestChecker;
}
