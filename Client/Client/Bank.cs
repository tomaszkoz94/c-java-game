﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Bank
    {
        public string name { get; set; }
        public double cost { get; set; }
        public int ammount { get; set; }

        public Bank(string name, double cost)
        {
            this.name = name;
            this.cost = cost;
        }

        public Bank(int ammount, string name)
        {
            this.ammount = ammount;
            this.name = name;
        }

        public void setCost(double cost)
        {
            this.cost = cost;
        }

        public void setAmmount(int ammount)
        {
            this.ammount = ammount;
        }

       
    }
}
