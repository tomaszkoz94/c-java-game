﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Model
    {
        private LinkedList<Services> servicesList;

        public Model()
        {
            servicesList = new LinkedList<Services>();
        }

        public LinkedList<Services> getServices()
        {
            return servicesList;
        }

        public void SetUserSomething(LinkedList<Services> userSth)
        {
            servicesList = userSth;
        }
    }
}
