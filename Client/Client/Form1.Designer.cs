﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.aliorCountb = new System.Windows.Forms.NumericUpDown();
            this.buyAlior = new System.Windows.Forms.Button();
            this.aliorLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.bosCountb = new System.Windows.Forms.NumericUpDown();
            this.citiCountb = new System.Windows.Forms.NumericUpDown();
            this.ideaCountb = new System.Windows.Forms.NumericUpDown();
            this.mbankCountb = new System.Windows.Forms.NumericUpDown();
            this.mileniumCountb = new System.Windows.Forms.NumericUpDown();
            this.pkoCountb = new System.Windows.Forms.NumericUpDown();
            this.bosLabel = new System.Windows.Forms.Label();
            this.citiLabel = new System.Windows.Forms.Label();
            this.ideaLabel = new System.Windows.Forms.Label();
            this.mbankLabel = new System.Windows.Forms.Label();
            this.mileniumLabel = new System.Windows.Forms.Label();
            this.pkoLabel = new System.Windows.Forms.Label();
            this.buyBos = new System.Windows.Forms.Button();
            this.buyCiti = new System.Windows.Forms.Button();
            this.buyIdea = new System.Windows.Forms.Button();
            this.buyMbank = new System.Windows.Forms.Button();
            this.buyMilenium = new System.Windows.Forms.Button();
            this.buyPko = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.totalMoneyLabel = new System.Windows.Forms.Label();
            this.EndTurnButton = new System.Windows.Forms.Button();
            this.sellAlior = new System.Windows.Forms.Button();
            this.sellBos = new System.Windows.Forms.Button();
            this.sellCiti = new System.Windows.Forms.Button();
            this.sellIdea = new System.Windows.Forms.Button();
            this.sellMbank = new System.Windows.Forms.Button();
            this.sellMilenium = new System.Windows.Forms.Button();
            this.sellPko = new System.Windows.Forms.Button();
            this.aliorCounts = new System.Windows.Forms.NumericUpDown();
            this.bosCounts = new System.Windows.Forms.NumericUpDown();
            this.citiCounts = new System.Windows.Forms.NumericUpDown();
            this.ideaCounts = new System.Windows.Forms.NumericUpDown();
            this.mbankCounts = new System.Windows.Forms.NumericUpDown();
            this.mileniumCounts = new System.Windows.Forms.NumericUpDown();
            this.pkoCounts = new System.Windows.Forms.NumericUpDown();
            this.aliorCLabel = new System.Windows.Forms.Label();
            this.bosCLabel = new System.Windows.Forms.Label();
            this.citiCLabel = new System.Windows.Forms.Label();
            this.ideaCLabel = new System.Windows.Forms.Label();
            this.mbankCLabel = new System.Windows.Forms.Label();
            this.mileniumCLabel = new System.Windows.Forms.Label();
            this.pkoCLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pkoOrder = new System.Windows.Forms.Label();
            this.millOrder = new System.Windows.Forms.Label();
            this.citiOrder = new System.Windows.Forms.Label();
            this.bosOrder = new System.Windows.Forms.Label();
            this.AliorOrder = new System.Windows.Forms.Label();
            this.totalOrder = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.mbankOrder = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ideaOrder = new System.Windows.Forms.Label();
            this.ConnectButton = new System.Windows.Forms.Button();
            this.waitForYourTurnLabel = new System.Windows.Forms.Label();
            this.failedToConnectLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.aliorCountb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bosCountb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.citiCountb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ideaCountb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mbankCountb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mileniumCountb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pkoCountb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aliorCounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bosCounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.citiCounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ideaCounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mbankCounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mileniumCounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pkoCounts)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // aliorCountb
            // 
            this.aliorCountb.Location = new System.Drawing.Point(208, 44);
            this.aliorCountb.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.aliorCountb.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.aliorCountb.Name = "aliorCountb";
            this.aliorCountb.Size = new System.Drawing.Size(71, 20);
            this.aliorCountb.TabIndex = 6;
            this.aliorCountb.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buyAlior
            // 
            this.buyAlior.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buyAlior.Location = new System.Drawing.Point(208, 70);
            this.buyAlior.Name = "buyAlior";
            this.buyAlior.Size = new System.Drawing.Size(71, 34);
            this.buyAlior.TabIndex = 7;
            this.buyAlior.Text = "BUY";
            this.buyAlior.UseVisualStyleBackColor = true;
            this.buyAlior.Click += new System.EventHandler(this.buyAlior_Click);
            // 
            // aliorLabel
            // 
            this.aliorLabel.AutoSize = true;
            this.aliorLabel.Font = new System.Drawing.Font("Impact", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aliorLabel.Location = new System.Drawing.Point(208, 19);
            this.aliorLabel.Name = "aliorLabel";
            this.aliorLabel.Size = new System.Drawing.Size(55, 22);
            this.aliorLabel.TabIndex = 8;
            this.aliorLabel.Text = "Price: ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 396);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(190, 90);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 14);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(190, 90);
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(12, 300);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(190, 90);
            this.pictureBox3.TabIndex = 11;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(12, 110);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(190, 90);
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(12, 205);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(190, 89);
            this.pictureBox5.TabIndex = 13;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(12, 492);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(190, 90);
            this.pictureBox6.TabIndex = 14;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(12, 587);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(190, 90);
            this.pictureBox7.TabIndex = 15;
            this.pictureBox7.TabStop = false;
            // 
            // bosCountb
            // 
            this.bosCountb.Location = new System.Drawing.Point(208, 140);
            this.bosCountb.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.bosCountb.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.bosCountb.Name = "bosCountb";
            this.bosCountb.Size = new System.Drawing.Size(71, 20);
            this.bosCountb.TabIndex = 16;
            this.bosCountb.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // citiCountb
            // 
            this.citiCountb.Location = new System.Drawing.Point(208, 234);
            this.citiCountb.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.citiCountb.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.citiCountb.Name = "citiCountb";
            this.citiCountb.Size = new System.Drawing.Size(71, 20);
            this.citiCountb.TabIndex = 17;
            this.citiCountb.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ideaCountb
            // 
            this.ideaCountb.Location = new System.Drawing.Point(208, 328);
            this.ideaCountb.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ideaCountb.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ideaCountb.Name = "ideaCountb";
            this.ideaCountb.Size = new System.Drawing.Size(71, 20);
            this.ideaCountb.TabIndex = 18;
            this.ideaCountb.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // mbankCountb
            // 
            this.mbankCountb.Location = new System.Drawing.Point(208, 426);
            this.mbankCountb.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mbankCountb.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mbankCountb.Name = "mbankCountb";
            this.mbankCountb.Size = new System.Drawing.Size(71, 20);
            this.mbankCountb.TabIndex = 19;
            this.mbankCountb.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // mileniumCountb
            // 
            this.mileniumCountb.Location = new System.Drawing.Point(208, 522);
            this.mileniumCountb.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mileniumCountb.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mileniumCountb.Name = "mileniumCountb";
            this.mileniumCountb.Size = new System.Drawing.Size(71, 20);
            this.mileniumCountb.TabIndex = 20;
            this.mileniumCountb.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pkoCountb
            // 
            this.pkoCountb.Location = new System.Drawing.Point(208, 617);
            this.pkoCountb.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.pkoCountb.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.pkoCountb.Name = "pkoCountb";
            this.pkoCountb.Size = new System.Drawing.Size(71, 20);
            this.pkoCountb.TabIndex = 21;
            this.pkoCountb.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // bosLabel
            // 
            this.bosLabel.AutoSize = true;
            this.bosLabel.Font = new System.Drawing.Font("Impact", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bosLabel.Location = new System.Drawing.Point(207, 115);
            this.bosLabel.Name = "bosLabel";
            this.bosLabel.Size = new System.Drawing.Size(55, 22);
            this.bosLabel.TabIndex = 22;
            this.bosLabel.Text = "Price: ";
            // 
            // citiLabel
            // 
            this.citiLabel.AutoSize = true;
            this.citiLabel.Font = new System.Drawing.Font("Impact", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.citiLabel.Location = new System.Drawing.Point(208, 209);
            this.citiLabel.Name = "citiLabel";
            this.citiLabel.Size = new System.Drawing.Size(55, 22);
            this.citiLabel.TabIndex = 23;
            this.citiLabel.Text = "Price: ";
            // 
            // ideaLabel
            // 
            this.ideaLabel.AutoSize = true;
            this.ideaLabel.Font = new System.Drawing.Font("Impact", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ideaLabel.Location = new System.Drawing.Point(208, 303);
            this.ideaLabel.Name = "ideaLabel";
            this.ideaLabel.Size = new System.Drawing.Size(55, 22);
            this.ideaLabel.TabIndex = 24;
            this.ideaLabel.Text = "Price: ";
            // 
            // mbankLabel
            // 
            this.mbankLabel.AutoSize = true;
            this.mbankLabel.Font = new System.Drawing.Font("Impact", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mbankLabel.Location = new System.Drawing.Point(207, 401);
            this.mbankLabel.Name = "mbankLabel";
            this.mbankLabel.Size = new System.Drawing.Size(55, 22);
            this.mbankLabel.TabIndex = 25;
            this.mbankLabel.Text = "Price: ";
            // 
            // mileniumLabel
            // 
            this.mileniumLabel.AutoSize = true;
            this.mileniumLabel.Font = new System.Drawing.Font("Impact", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mileniumLabel.Location = new System.Drawing.Point(208, 497);
            this.mileniumLabel.Name = "mileniumLabel";
            this.mileniumLabel.Size = new System.Drawing.Size(55, 22);
            this.mileniumLabel.TabIndex = 26;
            this.mileniumLabel.Text = "Price: ";
            // 
            // pkoLabel
            // 
            this.pkoLabel.AutoSize = true;
            this.pkoLabel.Font = new System.Drawing.Font("Impact", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pkoLabel.Location = new System.Drawing.Point(208, 588);
            this.pkoLabel.Name = "pkoLabel";
            this.pkoLabel.Size = new System.Drawing.Size(55, 22);
            this.pkoLabel.TabIndex = 27;
            this.pkoLabel.Text = "Price: ";
            // 
            // buyBos
            // 
            this.buyBos.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buyBos.Location = new System.Drawing.Point(208, 166);
            this.buyBos.Name = "buyBos";
            this.buyBos.Size = new System.Drawing.Size(71, 34);
            this.buyBos.TabIndex = 28;
            this.buyBos.Text = "BUY";
            this.buyBos.UseVisualStyleBackColor = true;
            this.buyBos.Click += new System.EventHandler(this.buyBos_Click);
            // 
            // buyCiti
            // 
            this.buyCiti.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buyCiti.Location = new System.Drawing.Point(208, 260);
            this.buyCiti.Name = "buyCiti";
            this.buyCiti.Size = new System.Drawing.Size(71, 34);
            this.buyCiti.TabIndex = 29;
            this.buyCiti.Text = "BUY";
            this.buyCiti.UseVisualStyleBackColor = true;
            this.buyCiti.Click += new System.EventHandler(this.buyCiti_Click);
            // 
            // buyIdea
            // 
            this.buyIdea.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buyIdea.Location = new System.Drawing.Point(208, 354);
            this.buyIdea.Name = "buyIdea";
            this.buyIdea.Size = new System.Drawing.Size(71, 34);
            this.buyIdea.TabIndex = 30;
            this.buyIdea.Text = "BUY";
            this.buyIdea.UseVisualStyleBackColor = true;
            this.buyIdea.Click += new System.EventHandler(this.buyIdea_Click);
            // 
            // buyMbank
            // 
            this.buyMbank.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buyMbank.Location = new System.Drawing.Point(208, 452);
            this.buyMbank.Name = "buyMbank";
            this.buyMbank.Size = new System.Drawing.Size(71, 34);
            this.buyMbank.TabIndex = 31;
            this.buyMbank.Text = "BUY";
            this.buyMbank.UseVisualStyleBackColor = true;
            this.buyMbank.Click += new System.EventHandler(this.buyMbank_Click);
            // 
            // buyMilenium
            // 
            this.buyMilenium.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buyMilenium.Location = new System.Drawing.Point(208, 548);
            this.buyMilenium.Name = "buyMilenium";
            this.buyMilenium.Size = new System.Drawing.Size(71, 34);
            this.buyMilenium.TabIndex = 32;
            this.buyMilenium.Text = "BUY";
            this.buyMilenium.UseVisualStyleBackColor = true;
            this.buyMilenium.Click += new System.EventHandler(this.buyMilenium_Click);
            // 
            // buyPko
            // 
            this.buyPko.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buyPko.Location = new System.Drawing.Point(208, 643);
            this.buyPko.Name = "buyPko";
            this.buyPko.Size = new System.Drawing.Size(71, 34);
            this.buyPko.TabIndex = 33;
            this.buyPko.Text = "BUY";
            this.buyPko.UseVisualStyleBackColor = true;
            this.buyPko.Click += new System.EventHandler(this.buyPko_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Segoe UI Black", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(380, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(281, 54);
            this.label1.TabIndex = 34;
            this.label1.Text = "Total money:";
            // 
            // totalMoneyLabel
            // 
            this.totalMoneyLabel.BackColor = System.Drawing.Color.Black;
            this.totalMoneyLabel.Font = new System.Drawing.Font("Segoe UI Black", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalMoneyLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.totalMoneyLabel.Location = new System.Drawing.Point(667, 12);
            this.totalMoneyLabel.Name = "totalMoneyLabel";
            this.totalMoneyLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.totalMoneyLabel.Size = new System.Drawing.Size(150, 54);
            this.totalMoneyLabel.TabIndex = 35;
            this.totalMoneyLabel.Text = "400";
            this.totalMoneyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // EndTurnButton
            // 
            this.EndTurnButton.BackColor = System.Drawing.Color.Black;
            this.EndTurnButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.EndTurnButton.FlatAppearance.BorderSize = 0;
            this.EndTurnButton.Font = new System.Drawing.Font("Segoe UI Black", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EndTurnButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.EndTurnButton.Location = new System.Drawing.Point(471, 376);
            this.EndTurnButton.Name = "EndTurnButton";
            this.EndTurnButton.Size = new System.Drawing.Size(247, 95);
            this.EndTurnButton.TabIndex = 36;
            this.EndTurnButton.Text = "End turn";
            this.EndTurnButton.UseVisualStyleBackColor = false;
            this.EndTurnButton.Click += new System.EventHandler(this.EndTurnButton_Click);
            // 
            // sellAlior
            // 
            this.sellAlior.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sellAlior.Location = new System.Drawing.Point(276, 70);
            this.sellAlior.Name = "sellAlior";
            this.sellAlior.Size = new System.Drawing.Size(80, 34);
            this.sellAlior.TabIndex = 38;
            this.sellAlior.Text = "SELL";
            this.sellAlior.UseVisualStyleBackColor = true;
            this.sellAlior.Click += new System.EventHandler(this.sellAlior_Click);
            // 
            // sellBos
            // 
            this.sellBos.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sellBos.Location = new System.Drawing.Point(276, 166);
            this.sellBos.Name = "sellBos";
            this.sellBos.Size = new System.Drawing.Size(80, 34);
            this.sellBos.TabIndex = 39;
            this.sellBos.Text = "SELL";
            this.sellBos.UseVisualStyleBackColor = true;
            this.sellBos.Click += new System.EventHandler(this.sellBos_Click);
            // 
            // sellCiti
            // 
            this.sellCiti.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sellCiti.Location = new System.Drawing.Point(276, 260);
            this.sellCiti.Name = "sellCiti";
            this.sellCiti.Size = new System.Drawing.Size(80, 34);
            this.sellCiti.TabIndex = 40;
            this.sellCiti.Text = "SELL";
            this.sellCiti.UseVisualStyleBackColor = true;
            this.sellCiti.Click += new System.EventHandler(this.sellCiti_Click);
            // 
            // sellIdea
            // 
            this.sellIdea.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sellIdea.Location = new System.Drawing.Point(276, 354);
            this.sellIdea.Name = "sellIdea";
            this.sellIdea.Size = new System.Drawing.Size(80, 34);
            this.sellIdea.TabIndex = 41;
            this.sellIdea.Text = "SELL";
            this.sellIdea.UseVisualStyleBackColor = true;
            this.sellIdea.Click += new System.EventHandler(this.sellIdea_Click);
            // 
            // sellMbank
            // 
            this.sellMbank.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sellMbank.Location = new System.Drawing.Point(276, 452);
            this.sellMbank.Name = "sellMbank";
            this.sellMbank.Size = new System.Drawing.Size(80, 34);
            this.sellMbank.TabIndex = 42;
            this.sellMbank.Text = "SELL";
            this.sellMbank.UseVisualStyleBackColor = true;
            this.sellMbank.Click += new System.EventHandler(this.sellMbank_Click);
            // 
            // sellMilenium
            // 
            this.sellMilenium.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sellMilenium.Location = new System.Drawing.Point(276, 548);
            this.sellMilenium.Name = "sellMilenium";
            this.sellMilenium.Size = new System.Drawing.Size(80, 34);
            this.sellMilenium.TabIndex = 43;
            this.sellMilenium.Text = "SELL";
            this.sellMilenium.UseVisualStyleBackColor = true;
            this.sellMilenium.Click += new System.EventHandler(this.sellMilenium_Click);
            // 
            // sellPko
            // 
            this.sellPko.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sellPko.Location = new System.Drawing.Point(276, 643);
            this.sellPko.Name = "sellPko";
            this.sellPko.Size = new System.Drawing.Size(80, 34);
            this.sellPko.TabIndex = 44;
            this.sellPko.Text = "SELL";
            this.sellPko.UseVisualStyleBackColor = true;
            this.sellPko.Click += new System.EventHandler(this.sellPko_Click);
            // 
            // aliorCounts
            // 
            this.aliorCounts.Location = new System.Drawing.Point(276, 44);
            this.aliorCounts.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.aliorCounts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.aliorCounts.Name = "aliorCounts";
            this.aliorCounts.Size = new System.Drawing.Size(80, 20);
            this.aliorCounts.TabIndex = 45;
            this.aliorCounts.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // bosCounts
            // 
            this.bosCounts.Location = new System.Drawing.Point(276, 140);
            this.bosCounts.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.bosCounts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.bosCounts.Name = "bosCounts";
            this.bosCounts.Size = new System.Drawing.Size(80, 20);
            this.bosCounts.TabIndex = 46;
            this.bosCounts.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // citiCounts
            // 
            this.citiCounts.Location = new System.Drawing.Point(276, 234);
            this.citiCounts.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.citiCounts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.citiCounts.Name = "citiCounts";
            this.citiCounts.Size = new System.Drawing.Size(80, 20);
            this.citiCounts.TabIndex = 47;
            this.citiCounts.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ideaCounts
            // 
            this.ideaCounts.Location = new System.Drawing.Point(276, 328);
            this.ideaCounts.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ideaCounts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ideaCounts.Name = "ideaCounts";
            this.ideaCounts.Size = new System.Drawing.Size(80, 20);
            this.ideaCounts.TabIndex = 48;
            this.ideaCounts.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // mbankCounts
            // 
            this.mbankCounts.Location = new System.Drawing.Point(276, 426);
            this.mbankCounts.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mbankCounts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mbankCounts.Name = "mbankCounts";
            this.mbankCounts.Size = new System.Drawing.Size(80, 20);
            this.mbankCounts.TabIndex = 49;
            this.mbankCounts.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // mileniumCounts
            // 
            this.mileniumCounts.Location = new System.Drawing.Point(276, 522);
            this.mileniumCounts.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mileniumCounts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mileniumCounts.Name = "mileniumCounts";
            this.mileniumCounts.Size = new System.Drawing.Size(80, 20);
            this.mileniumCounts.TabIndex = 50;
            this.mileniumCounts.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pkoCounts
            // 
            this.pkoCounts.Location = new System.Drawing.Point(276, 617);
            this.pkoCounts.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.pkoCounts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.pkoCounts.Name = "pkoCounts";
            this.pkoCounts.Size = new System.Drawing.Size(80, 20);
            this.pkoCounts.TabIndex = 51;
            this.pkoCounts.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // aliorCLabel
            // 
            this.aliorCLabel.AutoSize = true;
            this.aliorCLabel.BackColor = System.Drawing.Color.Black;
            this.aliorCLabel.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aliorCLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.aliorCLabel.Location = new System.Drawing.Point(169, 70);
            this.aliorCLabel.Name = "aliorCLabel";
            this.aliorCLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.aliorCLabel.Size = new System.Drawing.Size(33, 37);
            this.aliorCLabel.TabIndex = 52;
            this.aliorCLabel.Text = "0";
            this.aliorCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bosCLabel
            // 
            this.bosCLabel.AutoSize = true;
            this.bosCLabel.BackColor = System.Drawing.Color.Black;
            this.bosCLabel.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bosCLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.bosCLabel.Location = new System.Drawing.Point(169, 165);
            this.bosCLabel.Name = "bosCLabel";
            this.bosCLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bosCLabel.Size = new System.Drawing.Size(33, 37);
            this.bosCLabel.TabIndex = 53;
            this.bosCLabel.Text = "0";
            this.bosCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // citiCLabel
            // 
            this.citiCLabel.AutoSize = true;
            this.citiCLabel.BackColor = System.Drawing.Color.Black;
            this.citiCLabel.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.citiCLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.citiCLabel.Location = new System.Drawing.Point(169, 257);
            this.citiCLabel.Name = "citiCLabel";
            this.citiCLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.citiCLabel.Size = new System.Drawing.Size(33, 37);
            this.citiCLabel.TabIndex = 54;
            this.citiCLabel.Text = "0";
            this.citiCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ideaCLabel
            // 
            this.ideaCLabel.AutoSize = true;
            this.ideaCLabel.BackColor = System.Drawing.Color.Black;
            this.ideaCLabel.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ideaCLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ideaCLabel.Location = new System.Drawing.Point(169, 354);
            this.ideaCLabel.Name = "ideaCLabel";
            this.ideaCLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ideaCLabel.Size = new System.Drawing.Size(33, 37);
            this.ideaCLabel.TabIndex = 55;
            this.ideaCLabel.Text = "0";
            this.ideaCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // mbankCLabel
            // 
            this.mbankCLabel.AutoSize = true;
            this.mbankCLabel.BackColor = System.Drawing.Color.Black;
            this.mbankCLabel.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mbankCLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.mbankCLabel.Location = new System.Drawing.Point(169, 449);
            this.mbankCLabel.Name = "mbankCLabel";
            this.mbankCLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.mbankCLabel.Size = new System.Drawing.Size(33, 37);
            this.mbankCLabel.TabIndex = 56;
            this.mbankCLabel.Text = "0";
            this.mbankCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // mileniumCLabel
            // 
            this.mileniumCLabel.AutoSize = true;
            this.mileniumCLabel.BackColor = System.Drawing.Color.Black;
            this.mileniumCLabel.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mileniumCLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.mileniumCLabel.Location = new System.Drawing.Point(169, 547);
            this.mileniumCLabel.Name = "mileniumCLabel";
            this.mileniumCLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.mileniumCLabel.Size = new System.Drawing.Size(33, 37);
            this.mileniumCLabel.TabIndex = 57;
            this.mileniumCLabel.Text = "0";
            this.mileniumCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pkoCLabel
            // 
            this.pkoCLabel.AutoSize = true;
            this.pkoCLabel.BackColor = System.Drawing.Color.Black;
            this.pkoCLabel.Font = new System.Drawing.Font("Segoe UI Black", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pkoCLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pkoCLabel.Location = new System.Drawing.Point(169, 640);
            this.pkoCLabel.Name = "pkoCLabel";
            this.pkoCLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.pkoCLabel.Size = new System.Drawing.Size(33, 37);
            this.pkoCLabel.TabIndex = 58;
            this.pkoCLabel.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Segoe UI Black", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(471, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(247, 54);
            this.label2.TabIndex = 59;
            this.label2.Text = "Your order:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.pkoOrder, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.millOrder, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.citiOrder, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.bosOrder, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.AliorOrder, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.totalOrder, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.mbankOrder, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label7, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label9, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.ideaOrder, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(381, 163);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(427, 162);
            this.tableLayoutPanel1.TabIndex = 60;
            // 
            // pkoOrder
            // 
            this.pkoOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pkoOrder.AutoSize = true;
            this.pkoOrder.BackColor = System.Drawing.Color.Transparent;
            this.pkoOrder.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pkoOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pkoOrder.Location = new System.Drawing.Point(321, 120);
            this.pkoOrder.Name = "pkoOrder";
            this.pkoOrder.Size = new System.Drawing.Size(103, 42);
            this.pkoOrder.TabIndex = 74;
            this.pkoOrder.Text = "0";
            this.pkoOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // millOrder
            // 
            this.millOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.millOrder.AutoSize = true;
            this.millOrder.BackColor = System.Drawing.Color.Transparent;
            this.millOrder.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.millOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.millOrder.Location = new System.Drawing.Point(215, 120);
            this.millOrder.Name = "millOrder";
            this.millOrder.Size = new System.Drawing.Size(100, 42);
            this.millOrder.TabIndex = 73;
            this.millOrder.Text = "0";
            this.millOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // citiOrder
            // 
            this.citiOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.citiOrder.AutoSize = true;
            this.citiOrder.BackColor = System.Drawing.Color.Transparent;
            this.citiOrder.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.citiOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.citiOrder.Location = new System.Drawing.Point(321, 40);
            this.citiOrder.Name = "citiOrder";
            this.citiOrder.Size = new System.Drawing.Size(103, 40);
            this.citiOrder.TabIndex = 72;
            this.citiOrder.Text = "0";
            this.citiOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bosOrder
            // 
            this.bosOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bosOrder.AutoSize = true;
            this.bosOrder.BackColor = System.Drawing.Color.Transparent;
            this.bosOrder.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bosOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.bosOrder.Location = new System.Drawing.Point(215, 40);
            this.bosOrder.Name = "bosOrder";
            this.bosOrder.Size = new System.Drawing.Size(100, 40);
            this.bosOrder.TabIndex = 71;
            this.bosOrder.Text = "0";
            this.bosOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AliorOrder
            // 
            this.AliorOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AliorOrder.AutoSize = true;
            this.AliorOrder.BackColor = System.Drawing.Color.Transparent;
            this.AliorOrder.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AliorOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.AliorOrder.Location = new System.Drawing.Point(109, 40);
            this.AliorOrder.Name = "AliorOrder";
            this.AliorOrder.Size = new System.Drawing.Size(100, 40);
            this.AliorOrder.TabIndex = 70;
            this.AliorOrder.Text = "0";
            this.AliorOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // totalOrder
            // 
            this.totalOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.totalOrder.AutoSize = true;
            this.totalOrder.BackColor = System.Drawing.Color.Transparent;
            this.totalOrder.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.totalOrder.Location = new System.Drawing.Point(3, 40);
            this.totalOrder.Name = "totalOrder";
            this.totalOrder.Size = new System.Drawing.Size(100, 40);
            this.totalOrder.TabIndex = 69;
            this.totalOrder.Text = "0";
            this.totalOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 40);
            this.label10.TabIndex = 68;
            this.label10.Text = "TOTAL:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(215, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 40);
            this.label4.TabIndex = 62;
            this.label4.Text = "BOS";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(109, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 40);
            this.label3.TabIndex = 61;
            this.label3.Text = "ALIOR";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(321, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 40);
            this.label5.TabIndex = 63;
            this.label5.Text = "CITI";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mbankOrder
            // 
            this.mbankOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mbankOrder.AutoSize = true;
            this.mbankOrder.BackColor = System.Drawing.Color.Transparent;
            this.mbankOrder.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mbankOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.mbankOrder.Location = new System.Drawing.Point(109, 120);
            this.mbankOrder.Name = "mbankOrder";
            this.mbankOrder.Size = new System.Drawing.Size(100, 42);
            this.mbankOrder.TabIndex = 66;
            this.mbankOrder.Text = "0";
            this.mbankOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(3, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 40);
            this.label6.TabIndex = 64;
            this.label6.Text = "IDEA";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(109, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 40);
            this.label7.TabIndex = 65;
            this.label7.Text = "MBANK";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(215, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 40);
            this.label8.TabIndex = 66;
            this.label8.Text = "MILL";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(321, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 40);
            this.label9.TabIndex = 67;
            this.label9.Text = "PKO";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ideaOrder
            // 
            this.ideaOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ideaOrder.AutoSize = true;
            this.ideaOrder.BackColor = System.Drawing.Color.Transparent;
            this.ideaOrder.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ideaOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ideaOrder.Location = new System.Drawing.Point(3, 120);
            this.ideaOrder.Name = "ideaOrder";
            this.ideaOrder.Size = new System.Drawing.Size(100, 42);
            this.ideaOrder.TabIndex = 67;
            this.ideaOrder.Text = "0";
            this.ideaOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ConnectButton
            // 
            this.ConnectButton.BackColor = System.Drawing.Color.Black;
            this.ConnectButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ConnectButton.FlatAppearance.BorderSize = 0;
            this.ConnectButton.Font = new System.Drawing.Font("Segoe UI Black", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConnectButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ConnectButton.Location = new System.Drawing.Point(471, 542);
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(247, 95);
            this.ConnectButton.TabIndex = 61;
            this.ConnectButton.Text = "Connect";
            this.ConnectButton.UseVisualStyleBackColor = false;
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // waitForYourTurnLabel
            // 
            this.waitForYourTurnLabel.AutoSize = true;
            this.waitForYourTurnLabel.BackColor = System.Drawing.Color.Red;
            this.waitForYourTurnLabel.Font = new System.Drawing.Font("Segoe UI Black", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waitForYourTurnLabel.ForeColor = System.Drawing.Color.White;
            this.waitForYourTurnLabel.Location = new System.Drawing.Point(167, 369);
            this.waitForYourTurnLabel.Name = "waitForYourTurnLabel";
            this.waitForYourTurnLabel.Size = new System.Drawing.Size(421, 54);
            this.waitForYourTurnLabel.TabIndex = 62;
            this.waitForYourTurnLabel.Text = "Wait for your turn...";
            this.waitForYourTurnLabel.Visible = false;
            // 
            // failedToConnectLabel
            // 
            this.failedToConnectLabel.AutoSize = true;
            this.failedToConnectLabel.BackColor = System.Drawing.Color.Red;
            this.failedToConnectLabel.Font = new System.Drawing.Font("Segoe UI Black", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.failedToConnectLabel.ForeColor = System.Drawing.Color.White;
            this.failedToConnectLabel.Location = new System.Drawing.Point(449, 640);
            this.failedToConnectLabel.Name = "failedToConnectLabel";
            this.failedToConnectLabel.Size = new System.Drawing.Size(299, 28);
            this.failedToConnectLabel.TabIndex = 63;
            this.failedToConnectLabel.Text = "Failed to connect, try again...";
            this.failedToConnectLabel.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(824, 687);
            this.Controls.Add(this.failedToConnectLabel);
            this.Controls.Add(this.waitForYourTurnLabel);
            this.Controls.Add(this.ConnectButton);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pkoCLabel);
            this.Controls.Add(this.mileniumCLabel);
            this.Controls.Add(this.mbankCLabel);
            this.Controls.Add(this.ideaCLabel);
            this.Controls.Add(this.citiCLabel);
            this.Controls.Add(this.bosCLabel);
            this.Controls.Add(this.aliorCLabel);
            this.Controls.Add(this.pkoCounts);
            this.Controls.Add(this.mileniumCounts);
            this.Controls.Add(this.mbankCounts);
            this.Controls.Add(this.ideaCounts);
            this.Controls.Add(this.citiCounts);
            this.Controls.Add(this.bosCounts);
            this.Controls.Add(this.aliorCounts);
            this.Controls.Add(this.sellPko);
            this.Controls.Add(this.sellMilenium);
            this.Controls.Add(this.sellMbank);
            this.Controls.Add(this.sellIdea);
            this.Controls.Add(this.sellCiti);
            this.Controls.Add(this.sellBos);
            this.Controls.Add(this.sellAlior);
            this.Controls.Add(this.EndTurnButton);
            this.Controls.Add(this.totalMoneyLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buyPko);
            this.Controls.Add(this.buyMilenium);
            this.Controls.Add(this.buyMbank);
            this.Controls.Add(this.buyIdea);
            this.Controls.Add(this.buyCiti);
            this.Controls.Add(this.buyBos);
            this.Controls.Add(this.pkoLabel);
            this.Controls.Add(this.mileniumLabel);
            this.Controls.Add(this.mbankLabel);
            this.Controls.Add(this.ideaLabel);
            this.Controls.Add(this.citiLabel);
            this.Controls.Add(this.bosLabel);
            this.Controls.Add(this.pkoCountb);
            this.Controls.Add(this.mileniumCountb);
            this.Controls.Add(this.mbankCountb);
            this.Controls.Add(this.ideaCountb);
            this.Controls.Add(this.citiCountb);
            this.Controls.Add(this.bosCountb);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.aliorLabel);
            this.Controls.Add(this.buyAlior);
            this.Controls.Add(this.aliorCountb);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Stock Exchange (TK)";
            ((System.ComponentModel.ISupportInitialize)(this.aliorCountb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bosCountb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.citiCountb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ideaCountb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mbankCountb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mileniumCountb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pkoCountb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aliorCounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bosCounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.citiCounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ideaCounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mbankCounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mileniumCounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pkoCounts)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.NumericUpDown aliorCountb;
        public System.Windows.Forms.Button buyAlior;
        public System.Windows.Forms.Label aliorLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.NumericUpDown bosCountb;
        public System.Windows.Forms.NumericUpDown citiCountb;
        public System.Windows.Forms.NumericUpDown ideaCountb;
        public System.Windows.Forms.NumericUpDown mbankCountb;
        public System.Windows.Forms.NumericUpDown mileniumCountb;
        public System.Windows.Forms.NumericUpDown pkoCountb;
        public System.Windows.Forms.Label bosLabel;
        public System.Windows.Forms.Label citiLabel;
        public System.Windows.Forms.Label ideaLabel;
        public System.Windows.Forms.Label mbankLabel;
        public System.Windows.Forms.Label mileniumLabel;
        public System.Windows.Forms.Label pkoLabel;
        public System.Windows.Forms.Button buyBos;
        public System.Windows.Forms.Button buyCiti;
        public System.Windows.Forms.Button buyIdea;
        public System.Windows.Forms.Button buyMbank;
        public System.Windows.Forms.Button buyMilenium;
        public System.Windows.Forms.Button buyPko;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label totalMoneyLabel;
        private System.Windows.Forms.Button EndTurnButton;
        public System.Windows.Forms.Button sellAlior;
        public System.Windows.Forms.Button sellBos;
        public System.Windows.Forms.Button sellCiti;
        public System.Windows.Forms.Button sellIdea;
        public System.Windows.Forms.Button sellMbank;
        public System.Windows.Forms.Button sellMilenium;
        public System.Windows.Forms.Button sellPko;
        public System.Windows.Forms.NumericUpDown aliorCounts;
        public System.Windows.Forms.NumericUpDown bosCounts;
        public System.Windows.Forms.NumericUpDown citiCounts;
        public System.Windows.Forms.NumericUpDown ideaCounts;
        public System.Windows.Forms.NumericUpDown mbankCounts;
        public System.Windows.Forms.NumericUpDown mileniumCounts;
        public System.Windows.Forms.NumericUpDown pkoCounts;
        public System.Windows.Forms.Label aliorCLabel;
        public System.Windows.Forms.Label bosCLabel;
        public System.Windows.Forms.Label citiCLabel;
        public System.Windows.Forms.Label ideaCLabel;
        public System.Windows.Forms.Label mbankCLabel;
        public System.Windows.Forms.Label mileniumCLabel;
        public System.Windows.Forms.Label pkoCLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label mbankOrder;
        public System.Windows.Forms.Label ideaOrder;
        public System.Windows.Forms.Label pkoOrder;
        public System.Windows.Forms.Label millOrder;
        public System.Windows.Forms.Label citiOrder;
        public System.Windows.Forms.Label bosOrder;
        public System.Windows.Forms.Label AliorOrder;
        public System.Windows.Forms.Label totalOrder;
        private System.Windows.Forms.Button ConnectButton;
        private System.Windows.Forms.Label waitForYourTurnLabel;
        private System.Windows.Forms.Label failedToConnectLabel;
    }
}

