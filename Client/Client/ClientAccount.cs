﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class ClientAccount
    {
        private List<Bank> generalBankList;
        private List<Bank> userBankList;
        public double totalMoney { get; set; }

        public ClientAccount()
        {
            initAccount();
        }

        private void initAccount()
        {
            if (generalBankList == null || !generalBankList.Any())
            {

            }
            generalBankList = new List<Bank>();
            userBankList = new List<Bank>();
            totalMoney = 400;
        }

        public List<Bank> getGeneralBankList()
        {
            return generalBankList;
        }

        public List<Bank> getUserBankList()
        {
            return userBankList;
        }

        public double getTotalMoney()
        {
            return totalMoney;
        }

        public void setUserBankList(List<Bank> banks)
        {
            userBankList.Clear();
            userBankList.AddRange(banks);
            userBankList = banks;
        }
        public void setGeneralBankList(List<Bank> banks)
        {
            generalBankList.Clear();
            userBankList.AddRange(banks);
            generalBankList = banks;
        }

        public void setTotalMoney(double money)
        {
            totalMoney = money;
        }
    }
}
