﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace Client
{
    public partial class Form1 : Form
    {
        private readonly string serverAddress = "localhost";
        private readonly int port = 1133;
        static byte[] bytes = new byte[1024];
        XMLReader XMLreader;
        public ClientAccount clientAccount { get; set; }
        OrderService orderService;
        TcpClient client;
        NetworkStream stream;
        public List<Label> labelsInTable { get; }
        private List<Button> buyAndSellButtons;

        public Form1()
        {
            clientAccount = new ClientAccount();
            InitializeComponent();
            XMLreader = new XMLReader(this);
            labelsInTable = new List<Label>
            {
                AliorOrder, bosOrder, citiOrder, millOrder, mbankOrder, pkoOrder, ideaOrder, totalOrder
            };
            buyAndSellButtons = new List<Button>()
            {
                buyAlior, sellAlior, buyBos, sellBos, buyCiti, sellCiti, buyMilenium, sellMilenium, buyMbank, sellMbank, buyPko, sellPko, buyIdea, sellIdea 
            };
            BlockButtons(true);
            waitForYourTurnLabel.Visible = false;
            waitForYourTurnLabel.Refresh();
        }

        private void EndTurnButton_Click(object sender, EventArgs e)
        {
            BlockButtons(true);
            byte[] buf;
            //generate b001#S002... text
            string joined = orderService.getStringToSend();
            //send to server
            bool isConnect = false;
            buf = Encoding.UTF8.GetBytes(joined + Environment.NewLine);
            while (!isConnect)
            {
                try
                {
                    stream.Write(buf, 0, joined.Length + 2);
                    isConnect = true;
                }catch(SocketException){ }
            }
            buf = new byte[1024];
            //read stream
            isConnect = false;
            while (!isConnect)
            {
                try
                {
                    stream.Read(buf, 0, 1024);
                    isConnect = true;
                }
                catch (SocketException) { }
            }
            string readedStream = Encoding.UTF8.GetString(buf);
            Console.WriteLine(readedStream);
            string xml = Encoding.UTF8.GetString(buf);
            xml = xml.Substring(0, xml.IndexOf(char.ConvertFromUtf32(0)));

            //update form
            clientAccount = XMLreader.ReadReceivedData(xml);
            orderService = new OrderService(this, clientAccount);
            foreach (Label label in labelsInTable)
            {
                label.Text = "0";
            }
            BlockButtons(false);
        }

        private void BlockButtons(bool isBlocking)
        {
            if (isBlocking)
            {
                waitForYourTurnLabel.Visible = true;
                EndTurnButton.Enabled = false;
                foreach (Button button in buyAndSellButtons)
                {
                    button.Enabled = false;
                }

            }
            if (!isBlocking)
            {
                waitForYourTurnLabel.Visible = false;
                EndTurnButton.Enabled = true;
                foreach (Button button in buyAndSellButtons)
                {
                    button.Enabled = true;
                }
            }
        }

        private void appleButton_Click(object sender, EventArgs e)
        {
        }

        private void bananaButton_Click(object sender, EventArgs e)
        {
        }

        private void potatoButton_Click(object sender, EventArgs e)
        {
        }

        private void buyAlior_Click(object sender, EventArgs e)
        {
            orderService.buy(aliorCountb.Value, AliorOrder, 0);
        }

        private void buyBos_Click(object sender, EventArgs e)
        {
            orderService.buy(bosCountb.Value, bosOrder, 1);
        }

        private void buyCiti_Click(object sender, EventArgs e)
        {
            orderService.buy(citiCountb.Value, citiOrder, 2);
        }

        private void buyIdea_Click(object sender, EventArgs e)
        {
            orderService.buy(ideaCountb.Value, ideaOrder, 6);
        }

        private void buyMbank_Click(object sender, EventArgs e)
        {
            orderService.buy(mbankCountb.Value, mbankOrder, 4);
        }

        private void buyMilenium_Click(object sender, EventArgs e)
        {
            orderService.buy(mileniumCountb.Value, millOrder, 3);
        }

        private void buyPko_Click(object sender, EventArgs e)
        {
            orderService.buy(pkoCountb.Value, pkoOrder, 5);
        }
        private void sellAlior_Click(object sender, EventArgs e)
        {
            orderService.sell(aliorCounts.Value, AliorOrder, 0);
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            try
            {
                client = new TcpClient(serverAddress, port);
                stream = client.GetStream();
            }
            catch (SocketException)
            {
                failedToConnectLabel.Visible = true;
                return;
            }
            byte[] buf = new byte[1024];
            stream.Read(buf, 0, 1024);
            string readedStream = Encoding.UTF8.GetString(buf);
            Console.WriteLine(readedStream);


            string xml = Encoding.UTF8.GetString(buf);
            xml = xml.Substring(0, xml.IndexOf(char.ConvertFromUtf32(0)));

            //update form
            clientAccount = XMLreader.ReadReceivedData(xml);

            orderService = new OrderService(this, clientAccount);
            ConnectButton.Visible = false;
            failedToConnectLabel.Visible = false;
            BlockButtons(false);
        }

        private void sellBos_Click(object sender, EventArgs e)
        {
            orderService.sell(bosCounts.Value, bosOrder, 1);
        }

        private void sellCiti_Click(object sender, EventArgs e)
        {
            orderService.sell(citiCounts.Value, citiOrder, 2);
        }

        private void sellIdea_Click(object sender, EventArgs e)
        {
            orderService.sell(ideaCounts.Value, ideaOrder, 6);
        }

        private void sellMbank_Click(object sender, EventArgs e)
        {
            orderService.sell(mbankCounts.Value, mbankOrder, 4);
        }

        private void sellMilenium_Click(object sender, EventArgs e)
        {
            orderService.sell(mileniumCounts.Value, millOrder, 3);
        }

        private void sellPko_Click(object sender, EventArgs e)
        {
            orderService.sell(pkoCounts.Value, pkoOrder, 5);
        }
    }
}
