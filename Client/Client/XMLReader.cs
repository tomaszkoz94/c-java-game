﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace Client
{
    public class XMLReader
    {
        private Form1 form { get; }
        private ClientAccount clientAccount;
        XmlDocument doc;

        public XMLReader(Form1 form)
        {
            this.form = form;
            clientAccount = new ClientAccount();
        }


        internal ClientAccount ReadReceivedData(string dataToRead)
        {
            List<Bank> UserBankList = new List<Bank>();
            List<Bank> generalBankList = new List<Bank>();
            doc = new XmlDocument();
            doc.LoadXml(dataToRead);
            XmlNode nodes = doc.FirstChild;
            for (int i = 0; i < nodes.ChildNodes.Count; i++)
            {
                for (int j = 0; j < nodes.ChildNodes[i].ChildNodes.Count; j++)
                {
                    if (i == 0)
                    {
                        generalBankList.Add(new Bank(nodes.ChildNodes[i].ChildNodes[j].Name, double.Parse(nodes.ChildNodes[i].ChildNodes[j].InnerText)));
                    }
                    else
                    {
                        if (j == 7)
                        {
                            clientAccount.setTotalMoney(double.Parse(nodes.ChildNodes[i].ChildNodes[j].InnerText));
                        }
                        else
                        {
                            UserBankList.Add(new Bank(int.Parse(nodes.ChildNodes[i].ChildNodes[j].InnerText), nodes.ChildNodes[i].ChildNodes[j].Name));
                        }
                    }
                }
            }
            clientAccount.setGeneralBankList(generalBankList);
            clientAccount.setUserBankList(UserBankList);

            form.aliorLabel.Text = "Price = " + clientAccount.getGeneralBankList()[0].cost;
            form.bosLabel.Text = "Price = " + clientAccount.getGeneralBankList()[1].cost;
            form.citiLabel.Text = "Price = " + clientAccount.getGeneralBankList()[2].cost;
            form.mileniumLabel.Text = "Price = " + clientAccount.getGeneralBankList()[3].cost;
            form.mbankLabel.Text = "Price = " + clientAccount.getGeneralBankList()[4].cost;
            form.pkoLabel.Text = "Price = " + clientAccount.getGeneralBankList()[5].cost;
            form.ideaLabel.Text = "Price = " + clientAccount.getGeneralBankList()[6].cost;
            form.totalMoneyLabel.Text = clientAccount.getTotalMoney().ToString();

            form.aliorCLabel.Text = clientAccount.getUserBankList()[0].ammount.ToString();
            form.bosCLabel.Text = clientAccount.getUserBankList()[1].ammount.ToString();
            form.citiCLabel.Text = clientAccount.getUserBankList()[2].ammount.ToString();
            form.mileniumCLabel.Text = clientAccount.getUserBankList()[3].ammount.ToString();
            form.mbankCLabel.Text = clientAccount.getUserBankList()[4].ammount.ToString();
            form.pkoCLabel.Text = clientAccount.getUserBankList()[5].ammount.ToString();
            form.ideaCLabel.Text = clientAccount.getUserBankList()[6].ammount.ToString();

            form.aliorCounts.Maximum = clientAccount.getUserBankList()[0].ammount;
            form.bosCounts.Maximum = clientAccount.getUserBankList()[1].ammount;
            form.citiCounts.Maximum = clientAccount.getUserBankList()[2].ammount;
            form.mileniumCounts.Maximum = clientAccount.getUserBankList()[3].ammount;
            form.mbankCounts.Maximum = clientAccount.getUserBankList()[4].ammount;
            form.pkoCounts.Maximum = clientAccount.getUserBankList()[5].ammount;
            form.ideaCounts.Maximum = clientAccount.getUserBankList()[6].ammount;

            form.Refresh();
            return clientAccount;
        }
    }
}
