﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Client
{
    internal class OrderService
    {
        private ClientAccount clientAccount;
        Form1 form;
        List<Order> orders;
        double moneyBalance;

        public OrderService(Form1 form, ClientAccount clientAccount)
        {
            this.form = form;
            this.clientAccount = clientAccount;
            orders = new List<Order>();
            for (int i = 0; i < 7; i++)
            {
                orders.Add(new Order(OrderType.BUY, 0));
            }
            this.moneyBalance = clientAccount.totalMoney;
            
        }

        public string getStringToSend()
        {
            string stringToSend = string.Empty;
            char operandType = 'A';
            foreach(Order order in orders)
            {
                if (order.type == OrderType.BUY)
                {
                    operandType = 'B';
                }
                else
                {
                    operandType = 'S';
                }
                stringToSend += operandType + order.ammount.ToString("D3") + "#";
            }
            return stringToSend;
            //return string.Join("#", toSendList);
        }

        internal void buy(decimal value, Label labelToChange, int bankNumber)
        {
            double doubleValue = (double)value;
            double moneyBalance = clientAccount.totalMoney - clientAccount.getGeneralBankList()[bankNumber].cost * doubleValue;
            if (moneyBalance < 0) { return; }
            clientAccount.totalMoney = moneyBalance;
            orders[bankNumber] = new Order(OrderType.BUY, (int) doubleValue);
            int currentValue = int.Parse(labelToChange.Text);
            form.labelsInTable[bankNumber].Text = (clientAccount.getGeneralBankList()[bankNumber].ammount + currentValue + value).ToString();
            form.labelsInTable[bankNumber].Refresh();
            form.labelsInTable[7].Text = (double.Parse(form.labelsInTable[7].Text) - clientAccount.getGeneralBankList()[bankNumber].cost * doubleValue).ToString();
            form.labelsInTable[7].Refresh(); 
        }

        internal void sell(decimal value, Label labelToChange, int bankNumber)
        {
            double valueDouble = (double)value;
            moneyBalance += clientAccount.getGeneralBankList()[bankNumber].cost * valueDouble;
            orders[bankNumber] = new Order(OrderType.SELL, (int)valueDouble);
            int currentValue = int.Parse(labelToChange.Text);
            form.labelsInTable[bankNumber].Text = (clientAccount.getGeneralBankList()[bankNumber].ammount - currentValue - value).ToString();
            form.labelsInTable[bankNumber].Refresh();
            form.labelsInTable[7].Text = (double.Parse(form.labelsInTable[7].Text) + clientAccount.getGeneralBankList()[bankNumber].cost * valueDouble).ToString();
            form.labelsInTable[7].Refresh();
        }
    }
}