﻿namespace Client
{
    internal class Order
    {
        public OrderType type { get; }
        public int ammount { get; }

        public Order(OrderType type, int ammount)
        {
            this.type = type;
            this.ammount = ammount;
        }
    }
}